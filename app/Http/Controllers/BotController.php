<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BotController extends Controller
{
    /**
     * The verification token for Facebook
     *
     * @var string
     */
    protected $token;

    public function __construct()
    {
        $this->token = env('MESSENGER_VERIFY_TOKEN');
    }

    /**
     * Verify the token from Messenger. This helps verify your bot.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function verify_token(Request $request)
    {
        //dd($request);
        $mode  = $request->input('hub_mode');
        $token = $request->input('hub_verify_token');

        if ($mode === "subscribe" && $this->token and $token === $this->token) {
            return response($request->input('hub_challenge'),200);
        }

        return response("Invalid token!", 400);
    }

    /**
     * Handle the query sent to the bot.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function handle_query(Request $request)
    {
        $data = $request->all();
        //$reply = "test";
        //$sender  = array_get($entry, '0.messaging.0.sender.id');
        // $message = array_get($entry, '0.messaging.0.message.text');
        $sender = $data["entry"][0]["messaging"][0]["sender"]["id"];
        $message = $data["entry"][0]["messaging"][0]["message"];
        $message_text = $data["entry"][0]["messaging"][0]["message"]["text"];
        if (!empty($message)) {
            if ($message_text == "help") {
                $reply = "how can i help you?";
            } else if ($message_text == "hey") {
                $reply = "hey .. how are you";
            } else {
                $reply = "I can't understand you";
            }
            $this->dispatchResponse($sender, $reply);
        }
    }

    /**
     * Post a message to the Facebook messenger API.
     *
     * @param  integer $id
     * @param  string  $response
     * @return bool
     */
    protected function dispatchResponse($id, $response)
    {
        $access_token = env('PAGE_ACCESS_TOKEN');
        $url = "https://graph.facebook.com/v2.6/me/messages?access_token={$access_token}";

        $data = json_encode([
            'recipient' => ['id' => $id],
            'message'   => ['text' => $response]
        ]);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        $result = curl_exec($ch);
        curl_close($ch);


    }
}
