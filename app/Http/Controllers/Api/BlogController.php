<?php

namespace App\Http\Controllers\Api;

use App\Blog;
use App\Http\Requests\BlogRequest;
use App\Http\Resources\BlogCollection;
use App\Http\Resources\BlogResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{

    public function index()
    {
        $blogs = Blog::all();

        return new BlogCollection($blogs);
    }

    public function store(BlogRequest $request)
    {
        $input = $request->all();
        if($file = $request->file('image')){

            $name = $file->getClientOriginalName();

            $file->move('images', $name);
            // Storage::move($name);
            $input['image'] = $name;
        }
        dd($input);
        $blog = auth()->user()->blogs()->create($input);

        return (new BlogResource($blog))->additional([
            'message' => 'Blog created succesfully',
        ]);
    }

    public function listed($status)
    {
        $client = new \GuzzleHttp\Client();
        //$res = $client->request('GET', 'https://github.com');
        $request = $client->get('http://localhost/phpinfo.php');
        dd($request->getStatusCode());

        $client = new \GuzzleHttp\Client();

        dd($client);

        $request = $client->get('http://localhost:8000/api/blogs');

        $response = $request->getBody();

        dd($response);


        $blogs = Blog::where('status',$status)->get();

        return (new BlogCollection($blogs))->additional([
            'message' => Blog::$status[$status].' Blogs',
        ]);
    }
}
