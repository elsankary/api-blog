<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;


class LoginController extends Controller
{
    public $successStatus = 200;
    public $failStatus = 401;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['status'] = $this->successStatus;
            return response()->json(['success' => $success ,'user' => $user]);
        }
        else{
            $success['status'] = $this->failStatus;
            return response()->json(['success' => $success]);
        }
    }
}