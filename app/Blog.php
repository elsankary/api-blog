<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'brief', 'discription','status','user_id','image'
    ];


    /**
     * isActive Code.
     * @var integer
     */
    const DRAFT = 1;
    const PUBLISHED = 2;
    const CLOSED = 3;

    public function statusName()
    {
        $user_role = auth()->user()->role;
        switch ($user_role) {
            case 1:
                return 'draft';
                break;
            case 2:
                return 'published';
                break;
            case 3:
                return 'closed';
                break;
        }
    }

    /**
     * map each status code to a corresponding key name.
     *
     * @var array
     */
    public static $status = [
        1 => 'draft',
        2 => 'published',
        3 => 'closed',
    ];

    /**
     * blog belongs to one user
     * @return Relation
     */
    public function blogs(){
        return $this->belongsTo(User::class);
    }
}
