import Vue from 'vue'
import VueRouter from 'vue-router'

import store from './store'

import SignupPage from './components/auth/signup.vue'
import SigninPage from './components/auth/signin.vue'
import blogs from './components/blogs/index.vue'
import createBlog from './components/blogs/create.vue'
import chatBot from './components/chatbot/test.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/signup', component: SignupPage },
    { path: '/signin', component: SigninPage },
    { path: '/chatBot', component: chatBot },
    { path: '/blogs',
        component: blogs ,
        beforeEnter(to,from,next) {
            if (store.state.user_token) {
                next()
            } else {
                next('/signin')
            }
        }
    },
    { path: '/blogs/create',
        component: createBlog ,
        beforeEnter(to,from,next) {
            if (store.state.user_token) {
                next()
            } else {
                next('/signin')
            }
        }
    },
]

export default new VueRouter({mode: 'history', routes})