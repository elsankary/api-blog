import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from './router'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user_token: null,
        user: null,
        errors: null,
    },
    mutations: {
        authUser(state,userData) {
            state.user_token = userData.token,
            state.user = userData.user
        },

        authError(state,errorData) {
            state.errors = errorData.errorMessage
        }
    },
    actions: {
        signup({commit},authData) {
            axios.post('/api/register',authData).then((response) => {
                if (response.data.success.status == 200) {
                    commit('authUser', {
                        token: response.data.success.token,
                        user: response.data.user
                    })
                    router.push('/blogs')
                } else {
                    commit('authError', {
                        errorMessage: 'incorrect information',
                    })
                    //this.errors = 'incorrect information'
                }
            })
        },
        signin({commit},authData) {
            axios.post('/api/login',authData).then((response) => {
                if (response.data.success.status == 200) {
                    commit('authUser', {
                        token: response.data.success.token,
                        user: response.data.user
                    })
                    router.push('/blogs')
                } else {
                    commit('authError', {
                        errorMessage: 'incorrect email or password',
                    })
                    //this.errors = 'incorrect email or password'
                }
            })
        }
    },
    getters: {
        userInfo (state) {
            return state.user
        },
        token (state) {
            return state.user_token
        }
    }
})