<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\Auth\LoginController@login');
Route::post('register', 'API\Auth\RegisterController@register');

Route::group(['middleware' => 'auth:api'], function(){
    //blog routes
    Route::post('blogs/store','API\BlogController@store');
    Route::get('blogs','API\BlogController@index');
    Route::get('blogs/list/{status}','API\BlogController@listed');
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
